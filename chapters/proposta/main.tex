\chapter{Descoberta de Serviços Web via Linguagem Natural}
\label{cap:proposta}

Os Serviços Web Semânticos são descritos com a utilização de ontologias. Como as ontologias foram criadas por humanos, é possível que uma pessoa entenda do que se trata o serviço, porém usuários que não possuam o conhecimento necessário teriam dificuldade em entender. Para que os usuários comuns possam entender e requisitar o serviço, é importante que haja uma maneira de identificar o serviço através da linguagem natural \cite{sangers2013semantic}.

Dado o problema, este trabalho consiste na criação da ferramenta \emph{NLP Discovery} para realizar a descoberta de serviços utilizando a linguagem natural.

As seções desse capítulo são organizadas da seguinte maneira: A seção \ref{sec:arquitetura} mostra a arquitetura que foi definida e utilizada pela ferramenta construída; A Seção \ref{sec:ferramentasUtilizadas} mostra as ferramentas que foram utilizadas para auxiliar a construção do \emph{NLP Discovery}; A Seção \ref{sec:implementacao} descreve como foi feita a implementação e o algoritmo utilizado; Por fim, a seção \ref{sec:execucao} ilustra a execução da ferramenta.

\section{Arquitetura} \label{sec:arquitetura}

O \emph{NLP Discovery} foi desenvolvido para funcionar na Web e tem o seu visual como o de um site de buscas. A ferramenta foi desenvolvida com base em uma arquitetura pré-definida e possui os seus módulos desacoplados para facilitar futuras mudanças ou melhorias. A ferramenta está dividida em quatro módulos como ilustra a Figura \ref{fig:arquitetura2}.

\begin{figure}
\centering
\includegraphics[scale=0.5]{imagens/arquitetura.png}
\caption{Arquitetura do \emph{NLP Discovery}}
\label{fig:arquitetura2}
\end{figure} 

O módulo \emph{Semantic Reader} (Parte 1 da Figura \ref{fig:arquitetura2}) é a parte da aplicação que encapsula toda a leitura e extração da descrição dos serviços. Este módulo também possui a função de gerar uma abstração das descrições dos serviços apenas com as informações que serão utilizadas pelos outro módulos para realizar a descoberta dos serviços.

O módulo \emph{Disambiguator} (Parte 2 da Figura \ref{fig:arquitetura2}) vai receber as informações provindas do \emph{Semantic Reader} e também a sentença de entrada que é fornecida pelo usuário da aplicação. A função deste módulo é utilizar o algoritmo de Lesk mencionado na Seção \ref{sec:algoritmoLesk} do Capítulo \ref{cap:proclingnatural} juntamente com o Wordnet para retirar a ambiguidade das palavras da sentença do usuário e das palavras que compõe a abstração com as descrições dos serviços.

Ao chegar em \emph{Matching} (Parte 3 da Figura \ref{fig:arquitetura2}), as sentenças e os conjuntos de significados, tanto da consulta do usuário e das descrições dos serviços, são divididas em \emph{tokens} e comparadas utilizando o \textit{Coeficiente de Jaccard} apresentado na Seção \ref{sec:medidasSimilaridade}. Ao final da execução do módulo, é gerado um valor no intervalo de 0 e 1 que corresponde o grau de similaridade entre os dois conjuntos que foram comparados.

No módulo \emph{Web Service Discovery} (Parte 4 da Figura \ref{fig:arquitetura2}) todo o processo de descoberta já foi realizado e esse módulo fica encarregado de ordenar os serviços de acordo com o grau de similaridade com o que foi digitado pelo usuário e de exibir as informações de forma limpa e organizada para o usuário.

\section{Ferramentas utilizadas}
\label{sec:ferramentasUtilizadas}

Para a construção do \emph{NLP Discovery} foram utilizadas várias ferramentas, que serviram de suporte para a sua implementação e execução.

No módulo \emph{Semantic Reader} foi utilizada a RDF Parser\footnote{http://phpxmlclasses.sourceforge.net}, que é uma classe em PHP criada por Jason Williams e faz parte do projeto PHP XML Classes que consiste em um conjunto de classes para processar arquivos em XML utilizando PHP. Como as descrições da OWL-S estão no formato \ac{RDF}, é possível extrair de forma organizada todas as informações desejadas, utilizando a RDF Parser.

Para realizar a desambiguação semântica, é necessária a integração com o WordNet e é exatamente isso que a API \ac{JAWS}\footnote{http://lyle.smu.edu/~tspell/jaws/} realiza. A \ac{JAWS} é uma API que fornece a capacidade de recuperar dados do banco de dados WordNet para as aplicações escritas em JAVA.

O módulo \emph{Matching} teve a utilização da biblioteca NlpTools\footnote{http://php-nlp-tools.com/} que é uma biblioteca para processamento de linguagem natural escrita em PHP. A biblioteca fornece métodos para aplicar a tokenização nas sentenças e para calcular o coeficiente de Jaccard.

Para realizar a execução e teste da ferramenta, foi utilizada a \emph{OWL-S service retrieval test collection}\footnote{http://projects.semwebcentral.org/projects/owls-tc/} que contém 1083 descrições de serviços em \ac{OWL-S} e pode ser utilizada para avaliar o desempenho de algoritmos de descoberta de serviços.

\section{Implementação}
\label{sec:implementacao}

A implementação do \emph{NLP Discovery} foi dividida em várias partes, fazendo com que cada módulo funcionasse independente dos outros. As linguagens de programação utilizadas foram PHP e Java, ambas são linguagens populares e livres. As seções abaixo detalham a implementação e funcionamento de cada módulo.

\subsection{Semantic Reader}

O módulo \emph{Semantic Reader} é a parte que fica responsável em acessar as descrições dos serviços disponíveis e fornecer um arquivo contendo apenas as informações que serão utilizadas por outros módulos. Esse módulo foi implementado apenas utilizando a linguagem PHP.

A primeira parte, foi fazer a leitura das descrições de todos os serviços e para cada serviço, com a utilização da classe RDF Parser, extrair as informações necessárias. Para julgar quais informações eram necessárias, foi preciso analisar a estrutura do \ac{OWL-S} e verificar quais informações estavam ou poderiam ser convertidas em linguagem natural. Conforme ilustrado na Figura \ref{fig:owls-parser}, os dados extraídos foram:

\begin{itemize}
  \item{ \emph{serviceName}: Contém o nome do serviço. O nome do serviço muitas vezes está em CamelCase\footnote{CamelCase é a denominação em inglês para a prática de escrever palavras compostas ou frases, onde cada palavra é iniciada com letras maiúsculas e unidas sem espaços. Por exemplo, a palvra PreçoDoCafé se encontra em CamelCase.}  e nesse tipo de caso é necessário fazer a separação das palavras.}
  
  \item{ \emph{textDescription}: Contém um texto já em linguagem natural, que descreve resumidamente o que o serviço faz.  }
  
  \item{ \emph{hasInput} e \emph{hasOutput}: São os parâmetros de entrada e saída utilizados no serviço. Eles são classes que estão em ontologias \ac{OWL} que podem conter descrições em linguagem natural. 
  }
\end{itemize}

Após a extração, é gerado um arquivo que contém o nome, o nome com a retirada do CamelCase, a descrição em linguagem natural e as descrições dos parâmetros. Esse pré-processamento faz com que os outros módulos não precisem consultar diretamente a descrição de todos os serviços e com isso a descoberta torna-se muito mais rápida. 

\begin{figure}
\centering
\includegraphics[scale=0.5]{imagens/owls-parser.png}
\caption{Dados extraídos da descrição do serviço.}
\label{fig:owls-parser}
\end{figure}

\subsection{Disambiguator}

Um grande problema ao processar textos que estão na linguagem natural, é o problema da ambiguidade que foi mencionado na Seção \ref{sec:desabiguacaoSemantica} do Capítulo \ref{cap:proclingnatural}. O módulo \emph{Disambiguator} recebe a entrada do usuário e o arquivo gerado pelo módulo \emph{Semantic Reader} e tem como tarefa, enviar para o próximo módulo, os possíveis significados de todas palavras contidas em ambas entradas.

O algoritmo de Lesk foi implementado na linguagem Java, utilizando também a API \ac{JAWS} para se comunicar com o WordNet. O algoritmo implementado teve algumas alterações em relação ao algoritmo original criado por Lesk. A "assinatura", além de conter a lista de palavras que aparecem na definição da palavra ambígua, contém também a lista de palavras que aparecem nos exemplos de uso da palavra ambígua em frases alheias. Outra alteração é que antes de processar o algoritmo, as \emph{stop words} e os caracteres especiais são removidos.

O Código Fonte \ref{cod:lesk} representa o algoritmo de Lesk que foi implementado. Inicialmente, o método \textit{doLesk} recebe dois parâmetros, \emph{word} e \emph{sentence}, que representam a palavra ambígua e a sentença completa em que a palavra está contida respectivamente. Após o início do método, são executadas duas funções que tratam a sentença, \emph{removeSpecialCharacters} que remove os caracteres especiais, já que estes não representam nenhum significado semântico e a função \emph{removeStopWords} para retirar as \emph{stop words}, pois as mesmas são comumente usadas e na maioria das vezes não tem valor significativo para o contexto. As palavras da sentença são separadas e então armazenadas no vetor \emph{wordsOfSentence} com a exceção da palavra ambígua. A variável \emph{bestSense} primeiro armazena o sentido mais utilizado, segundo o WordNet, para a palavra ambígua e \textit{signatures} guarda todas as "assinaturas" para aquela palavra. Para toda "assinatura", é executado o método \emph{computOverlap} que retorna a quantidade de palavras em comum entre a "assinatura" e a sentença de entrada fornecida pelo usuário. Por último, é selecionado o significado que representa o maior \emph{overlap} calculado, esse vai ser o significado atribuído aquela palavra no contexto em que está inserida.

\begin{lstlisting}[caption=Algoritmo de Lesk em Java, label=cod:lesk, language=JAVA, frame=single]
public class Lesk {

    public String doLesk(String word, String sentence){
        ArrayList<ArrayList<String>> signatures;
        ArrayList<String> wordsOfSentence
                           = new ArrayList<String>();
        String bestSense;
        int maxOverlap = 0;
        int overlap;

        sentence = removeSpecialCharacters(sentence);
        sentence = removeStopWords(sentence);
        String[] tokens = sentence.split(" ");
        for (int j = 0; j < tokens.length; j++){
            if(!tokens[j].equals(word)){
                wordsOfSentence.add(tokens[j]);
            }
        }
        WordNet wordnet = new WordNet();
        signatures = wordnet.getSignatures(word);
       	bestSense = wordnet.getBestSense(word);
       	
        for(int i=0;i<words.size();i++){
            overlap = computeOverlap(signatures.get(i),
                              wordsOfSentence);
            if(overlap > maxOverlap){
                maxOverlap = overlap;
                bestSense = wordnet
                  .getBestSenseForSignature(signatures
                  .get(i));
            }
        }
        return bestSense;
    }

    public int computeOverlap(ArrayList<String> dictionary
                         , ArrayList<String> context){
        int i,j,cont=0;

        for(i=0;i<context.size();i++){
            for(j=1;j<dictionary.size();j++){
                if(context.get(i).toLowerCase()
                   .equals(dictionary.get(j).toLowerCase()
                )){
                    cont++;
                    break;
                }
            }
        }
        return cont;
    }
}
\end{lstlisting}

\subsection{Matching}

O módulo \emph{Matching} recebe como saída do módulo anterior, a sentença digitada pelo usuário e as informações relevantes dos serviços, ambos com os seus respectivos conjuntos de significados gerados no processo de desambiguação semântica. Nessa etapa, ocorre uma comparação do que o usuário digitou com os serviços, para poder avaliar quais serviços que atenderão à consulta do usuário. Esse módulo foi implementado apenas utilizando a linguagem PHP.

Os dados extraídos dos serviços recebem um peso que é proporcional a quanto aquela informação consegue descrever algo relevante para o serviço. Os pesos foram encontrados empiricamente através de tentativa e erro, os pesos devem totalizar no valor 1 e foram definidos da seguinte maneira:

\begin{itemize}
	\item{Nome do serviço: 0.4}
	\item{Descrição textual do serviço: 0.4}
	\item{Nome dos parâmetros: 0.1}
	\item{Descrição dos parâmetros: 0.1}
\end{itemize}

Cada item é composto pelo texto que foi extraído e outro texto contendo os significados das palavras. Para calcular o grau de similaridade para cada item extraído, foi aplicado o cálculo do \emph{Coeficiente de Jaccard} (\emph{JDT}) em relação a entrada do usuário e realizada uma média ponderada com os pesos de 0.4 para o texto extraído (\emph{jaccardItem}) e 0.6 para o texto com os significados (\emph{jaccardItemSenses}), como mostrado na Fórmula \ref{eq:formula1}.


\begin{equation}
\label{eq:formula1}
JDT(x) = (jaccardItem(x) * 0.4) + (jaccardItemSenses(x) * 0.6)
\end{equation}

Como esse cálculo é feito para cada item, é preciso calcular um valor final correspondente ao resultado de cada um dos quatros itens. O cálculo é feito realizando uma média ponderada com os pesos anteriormente citados. O valor final, varia entre 0 e 1; quanto mais próximo de 1, mais o serviço é similar ao valor digitado pelo usuário. O cálculo é mostrado na Fórmula \ref{eq:formula2}.

\begin{equation}
\label{eq:formula2}
jacardValue = 
(JDT(1) * 0.4) + (JDT(2) * 0.4) + (JDT(3) * 0.1) + (JDT(4) * 0.1)
\end{equation}

Por fim, cada serviço vai estar associado a um \emph{jaccardValue} e essas informações serão enviadas para o módulo \emph{Web Service Discovery}.

\subsection{Web Service Discovery}

O módulo \emph{Web Service Discovery} recebe a lista de todos os serviços com os respectivos valores do coeficiente de Jaccard calculado. Os resultados são ordenados em ordem decrescente fazendo com que seja apresentado ao usuário os serviços mais similares primeiro. Esse módulo foi implementado apenas utilizando a linguagem PHP.

\section{Interface}

O \emph{NLP Discovery} foi construído para rodar na Web. Com a utilização do Bootstrap\footnote{http://getbootstrap.com/}, que é uma coleção gratuita de ferramentas para a criação de sites e aplicações Web. A aplicação tem um visual muito simples e limpo, contando com versões para desktop e mobile, conforme as Figuras \ref{fig:desktop} e \ref{fig:mobile}.  

\begin{figure}
\centering
\includegraphics[scale=0.4]{imagens/desktop.png}
\caption{Versão para desktop do \emph{NLP Discovery}}
\label{fig:desktop}
\end{figure} 

\begin{figure}
\centering
\includegraphics[scale=0.3]{imagens/mobile.png}
\caption{Versão mobile do \emph{NLP Discovery}}
\label{fig:mobile}
\end{figure} 

\section{Execução}
\label{sec:execucao}

A primeira parte da execução é o pré-processamento, que não é visto pelo usuário e serve para gerar o arquivo com as informações relevantes dos serviços. Esse pré-processamento é executado via linha de comando e exibe a quantidade de serviços que já foram processados.

A segunda parte é execução da aplicação que é utilizada pela usuário. A aplicação dispõe de uma caixa de pesquisa e um botão para realizá-la. Após pesquisar é mostrado ao usuário uma lista com os serviços e oferece a possibilidade do usuário acessar a descrição em \ac{OWL-S} de qualquer serviço. Esse fluxo é ilustrado na Figura \ref{fig:fluxo}.

\begin{figure}
\centering
\includegraphics[scale=0.6]{imagens/fluxo.png}
\caption{Fluxo de execução do \emph{NLP Discovery}}
\label{fig:fluxo}
\end{figure} 

