\chapter{Processamento de linguagem natural}
\label{cap:proclingnatural}

A linguagem é todo sistema do qual é possível extrair significados capazes de estabelecer a comunicação entre os diversos sistemas existentes. Por exemplo, comunicação entre linguagens naturais com artificiais ou linguagens verbais com não verbais. Para todos os tipos de linguagens sempre é encontrado uma forma para produzir um significado que formaliza a comunicação entre os diversos sistemas \citep{monografiaPLN}. 

Para se desenvolver recursos capazes de tratar destes problemas, é necessário descobrir como os homens se comunicam. Porém, isto não é tão simples assim, pois ainda não há conclusões científicas definidas de como o cérebro humano trabalha o entendimento das línguas humanas. Existem duas linhas de pesquisa que delineiam esta área, a Linguística (enfoque da gramática gerativa 
transformacional) e a Psicologia Cognitiva (procurando detectar unidades temáticas como: 
metas, intenções, consequências e causas, existentes em textos para realizar o processamento) \citep{monografiaPLN}.


O \ac{PLN} é um ramo da Inteligência Artificial que estuda o desenvolvimento de um sistema computacional que analisa e tenta compreender ou produzir uma ou mais línguas humanas naturais. Um sistema que realiza o \ac{PLN} poderia ser uma aplicação que traduz um texto para outro idioma, que compreendesse e representasse um conteúdo de um texto ou que realizasse uma conversa com um usuário \citep{Allen:2003:NLP:1074100.1074630}.

O \ac{PLN} faz com que os seres humanos consigam se comunicar com computadores de uma forma mais natural, utilizando a mesma linguagem que costumam usar para se comunicar no seu dia a dia.

Este capítulo descreve o que é uma linguagem e como funciona o processamento de uma linguagem natural. Também é mostrado nesse capítulo, os problemas comumente encontrados e algoritmos que servem para tratar esses problemas. 

As seções deste capítulo são organizadas da seguinte maneira: A Seção \ref{sec:desabiguacaoSemantica} define o problema da ambiguidade e mostra um algoritmo que trata desse problema; A Seção \ref{sec:medidasSimilaridade} fala sobre os algoritmos de similaridade e como são usados na área de \ac{PLN}. 

\section{Desambiguação semântica} \label{sec:desabiguacaoSemantica}

A principal dificuldade encontrada no \ac{PLN} é a ambiguidade encontrada em todos os níveis do problema. Por exemplo, uma linguagem natural envolve:
\begin{itemize}
  \item{\textbf{Ambiguidade lexical}: Ocorre quando uma palavra pode possuir mais de um significado em um determinado contexto.
  Por exempplo: "Os ministros devem aterrar a qualquer momento."
  (aterrar = causar terror / aterrar = aproximar-se do solo).
  }
  \item{\textbf{Ambiguidade sintática}: Ocorre quando podemos interpretar uma sentença de várias formas mesmo que as palavras não possuam duplo sentido. Por exemplo: "Eu vi um homem com um telescópio" (Não é possível saber a quem o telescópio pertence)}
  \item{\textbf{Ambiguidade semântica}: Resulta dos inúmeros significados que uma palavra pode possuir.}
  \item{\textbf{Ambiguidade pragmática}: Esta não se relaciona a forma lógica da sentença, e sim ao uso que pode-se fazer dela. Por exemplo: "Você pode levantar a pedra?" (Pode ser uma pergunta que espera um sim/não como resposta ou apenas um pedido)}
\end{itemize}

Esses tipos de ambiguidades podem interagir entre si e gerar sentenças que possuam uma interpretação extremamente complexa. É a prevalência da ambiguidade que distingue as linguagens naturais das linguagens artificiais, como lógica e linguagens de programação \citep{Allen:2003:NLP:1074100.1074630}.

Segundo \cite{Ide:1998:ISI:972719.972721}, a tarefa da desambiguação semântica consiste em associar a uma palavra ambígua em uma sentença ou texto um sentido que é distinguível dos outros sentidos potencialmente atribuíveis a tal palavra, de acordo com o contexto dessa palavra.

\subsection{Algoritmo de Lesk} \label{sec:algoritmoLesk}

O trabalho de \cite{lesk1986automatic}, voltado para a Recuperação de Informações, mostra que o uso de um dicionário eletrônico\footnote{No inglês Machine-readable dictionary, é um dicionário que também pode ser consultado através de um software.} consistente deve permitir a desambiguação de uma palavra, verificando as palavras disponíveis nas definições dos sentidos e comparando com as outras palavras presentes no contexto da sentença \citep{specia2004desambiguaccao}.

Para verificar sua hipótese, o autor associou uma "assinatura" a cada sentido de uma palavra ambígua. Essa assinatura consiste em uma lista de palavras que aparecem na definição de um dicionário do sentido da palavra ambígua. A desambiguação é realizada pela seleção do sentido da palavra ambígua cuja assinatura apresenta maior similaridade com as assinaturas das palavras vizinhas da sentença \citep{Ide:1998:ISI:972719.972721}.

Lesk não realizou uma avaliação sistemática do seu trabalho, mas apenas alguns testes com pequenos exemplos. Nesses testes, ele obteve uma acurácia de 50\% a 70\%. Segundo ele, esses resultados são significativos, considerando-se que são empregadas distinções de sentido relativamente refinado (dadas pelo dicionário), que não houve dependência de contexto global e que apenas as informações disponíveis no dicionário são utilizadas. Ele sugere que essa abordagem seja utilizada como complemento a algum mecanismo que utilize outras informações, como as relações sintáticas entre as palavras. Um problema com esse trabalho é que ele é dependente das definições de um dicionário específico: a presença ou ausência de uma palavra na definição daquele dicionário pode mudar completamente os resultados. Além disso, a simples contagem das palavras nas assinaturas pode privilegiar a escolha de sentidos com definições mais extensas \citep{specia2004desambiguaccao}.

\subsection{WordNet} 
\label{sec:wordNet}

Como afirmado na seção \ref{sec:algoritmoLesk}, o algoritmo de Lesk utiliza de um dicionário para retornar os possíveis significados e até exemplos de uso de uma palavra específica. 

WordNet é um sistema lexical desenvolvido manualmente e construído por George Miller no Laboratório de Ciência Cognitiva da Universidade de Princeton. Originado de um projeto cujo objetivo era produzir um dicionário que pudesse ser pesquisado conceitualmente, e não apenas em ordem alfabética, WordNet evoluiu para um sistema que reflete teorias psicolinguísticas atuais de como o ser humano organiza suas memórias lexicais. O objeto básico no WordNet é um conjunto de sinônimos chamados \emph{synset}. Por definição, cada \textit{synset} em que uma palavra aparece é um sentido diferente da mesma.

Existem quatro divisões principais no WordNet, que são divididas para substantivos, verbos, adjetivos e advérbios. Dentro de cada divisão, os \textit{synsets} são organizados através das relações léxicas entre eles. As relações léxicas incluem as relações "IS-A"\footnote{ No português "É-UM", define um relacionamento onde diz que uma classe A é sub-classe de outra classe B.} e "PART-OF"\footnote{ No português "PARTE-DE", define um relacionamento onde diz que dada duas classes A e B, se B é parte de A, significa que se B existe A também existe e B é parte de A, mas o fato de A existir não implica em B existir.}. A relação "IS-A" é a relação dominante, e organiza os \textit{synsets} em um conjunto de cerca de dez hierarquias. A Figura \ref{fig:wordnet} mostra a organização das léxicas através das relações "IS-A". Na Figura, cada quadro representa um \textit{synset} e os quadros cinzas representam significados diferentes para o substantivo \emph{board}. É possível ver a hierarquia das relações "IS-A", por exemplo, sete dos oito significados da palavra \emph{board} são sub-classes da classe \emph{object} \citep{Voorhees:1993:UWD:160688.160715}.

O WordNet pode ser utilizado pelo algoritmo de Lesk no momento em que é necessário retornar todos os sentidos de uma palavra ou até para retornar os exemplos de uso daquela palavra em sentenças comuns.

\begin{figure}
\centering
\includegraphics[scale=0.7]{imagens/wordnet.png}
\caption{Hierarquia "IS-A" relacionando oito diferentes significados para o substantivo board \cite{Voorhees:1993:UWD:160688.160715}}
\label{fig:wordnet}
\end{figure} 

\section{Medidas de similaridade} 
\label{sec:medidasSimilaridade}

Medidas de similaridade servem para calcular o grau de similaridade entre dois objetos. No campo da estatística pode-se encontrar algoritmos que podem ser expandidos para outras áreas, como por exemplo, a área de \ac{PLN}.

Dois algoritmos que são comumente utilizados para comparar sentenças em linguagem natural, são os algoritmos que calculam o \emph{Coeficiente de Jaccard} e \emph{Similaridade do Cosseno}.

O \emph{Coeficiente de Jaccard} mede a similaridade entre dois conjuntos, calculando o tamanho da interseção dividido pela união desses conjuntos. A fórmula matemática para o cálculo do \emph{Coeficiente de Jaccard} está representado na Fórmula \ref{eq:formulaJaccard} e tem como resultado um valor numérico entre 0 e 1.

\begin{equation}
\label{eq:formulaJaccard}
J(A, B) = \frac{|A \cap B|}{|A \cup B|}
\end{equation}

\begin{equation}
\label{eq:formulaJaccard2}
0 \leq J(A, B) \leq 1
\end{equation}