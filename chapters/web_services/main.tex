\chapter{Serviços Web} 
\label{cap:servicosWeb}

Um Serviço Web é um software que pode ser acessado remotamente usando diferentes Linguagens baseadas em \ac{XML}. Normalmente, um Serviço Web é identificado por uma URL, assim como qualquer outro site. O que torna os Serviços Web diferentes de sites comuns é o tipo de interação que eles podem fornecer \citep{kopack2003sams}.

Com o uso dos Serviços Web, é possível que novas aplicações possam interagir com aplicações já existentes, desde que essas aplicações ofereçam serviços, e que sistemas desenvolvidos em diferentes plataformas sejam compatíveis. O objetivo dos Serviços Web é a comunicação de aplicações através da internet.

Este capítulo descreve os tipos de arquiteturas dos Serviços Web, fazendo uma comparação entre as arquiteturas WS-* e \ac{REST}. Também é explicado o que é a Web Semântica e o que são Serviços Web Semânticos. 

As seções desse capítulo são organizadas da seguinte maneira: A Seção \ref{sec:tiposServicos} define os Serviços Web de acordo com as arquiteturas que podem ser utilizadas; A Seção \ref{sec:servicossemanticos} explica o que são os Serviços Web Semânticos e mostra o uso de ontologias para descrever Serviços Web. 

\section{Tipos de Serviços Web} \label{sec:tiposServicos}

Serviços Web podem utilizar a arquitetura WS-*, que são Serviços Web que que utilizam \ac{SOAP} e demais especificações como \ac{WSDL}, ou a arquitetura \ac{REST}.

\ac{SOAP} é um protocolo leve para troca de informações em um ambiente descentralizado. É um protocolo baseado em \ac{XML} que consiste em três partes: um envelope que define um quadro para descrever o que está em uma mensagem e como processá-la; um conjunto de regras de codificação para expressar instâncias de tipos de dados definidos pela aplicação e uma convenção para representar as requisições e respostas. \ac{SOAP} pode potencialmente ser usado em combinação com uma variedade de outros protocolos \citep{soap}.

\ac{REST} surgiu a partir de uma tese de doutorado de \cite{Fielding:2000:ASD:932295}. O \ac{REST} utiliza o protocolo \ac{HTTP} para trocar mensagens. O \ac{REST} utiliza os métodos POST, GET, PUT e DELETE do protocolo \ac{HTTP} e que fazem uma analogia com o CRUD (\textit{Create, Read, Update e Delete}), que são as quatro operações básicas utilizadas em banco de dados relacionais \citep{bigolinrest}. Uma comparação entre as arquiteturas pode ser encontrada na Tabela \ref{table:restvcsoap}.

\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|}
\hline
                     & WS-*                  & REST               \\ \hline
Descrição do serviço & WSDL                  & Nenhum padronizado \\
Descoberta           & UDDI                  & Não possui         \\
Comunicação          & Síncrona / Assíncrona & Sincrona           \\
Estado               & Com estado            & Sem estado         \\
Protocolos           & Vários                & Somente HTTP       \\
Multilinguagem       & SIM                   & SIM                \\
Bibliotecas XML      & SIM                   & Não necessário     \\
Segurança            & HTTPS / WS-Security & HTTPS                \\
Formato de Mensagem  & SOAP / XML            & Não padronizada    \\ \hline
\end{tabular}
\caption {Comparação das arquiteturas WS-* e REST \cite{bigolinrest}}
\label{table:restvcsoap}
\end{table}

\section{Serviços Web Semânticos} \label{sec:servicossemanticos}

A maioria do conteúdo da Web foi projetado para permitir que o conteúdo fosse lido por seres humanos e não por programas de computador. Os computadores podem analisar páginas da Web, mas, em geral, os computadores não tem nenhuma maneira padronizada para processar a semântica da página. A Web Semântica serve para trazer uma estrutura significativa para o conteúdo de páginas da Web, criando um ambiente onde programas de computador possam processar e entender a informação contida na página. A Web Semântica não está separada da Web, sendo uma extensão da mesma, em que a informação possui um significado bem definido, facilitando o trabalho cooperativo entre computadores e pessoas.

Os Serviços Web, apresentam deficiências nas áreas de descrição, descoberta e composição de serviços. A deficiência é causada devido a falta de suporte semântico no \ac{WSDL} e no mecanismo de armazenamento e pesquisa de serviços. Com o objetivo de integrar a Web Semântica aos Serviços Web, foram desenvolvidas linguagens para descrição e composição de serviços.


\subsection{Ontologias}

Na Web Semântica é possível encontrar coleções de informações chamadas de ontologias. Ontologias são utilizadas em Web Semântica para representar o conhecimento sobre o mundo ou alguma parte deste. O tipo mais comum de ontologia para a Web possui uma taxonomia e um conjunto de regras de inferência. 

A taxonomia define uma classe de objetos e as relações entre eles. Classes, subclasses e relações entre entidades são ferramentas poderosas para o uso da Web. É possível expressar um grande número de relações entre entidades atribuindo propriedades a classes e permitindo que subclasses herdem cada propriedade. 

Por exemplo, se uma ontologia possui a seguinte regra: "Se uma cidade é associada a um código de estado, e um endereço usa o código de uma cidade, então esse endereço está associado ao código do estado". Um programa poderia deduzir que o endereço da Universidade de Cornell, que está na cidade de Ithaca, tem que estar no estado de Nova York, que está nos Estados Unidos e, portanto, deve ser formatado nos padrões do país. O computador não entende realmente o que a frase quer dizer, mas através de inferência consegue manipular os termos de forma que se torne mais eficaz para o usuário
\citep{berners2001semantic}.

A \ac{OWL} é uma linguagem para a descrição de ontologias, fazendo com que humanos e computadores compreendam o conteúdo descrito. As ontologias contêm vocabulário sobre um conceito pré-definido, para representar o conhecimento. Uma ontologia OWL é composta por indivíduos, propriedades e classes, como detalhado abaixo:

\begin{itemize}

\item{\textbf{Indivíduos}: Estes representam objetos no domínio de interesse. A \ac{OWL} não usa o conceito \ac{UNA}, isto significa que 
dois nomes diferentes podem se referir ao mesmo indivíduo. Por exemplo, "Rainha Elizabeth", "A Rainha" e " Elizabeth Windsor" podem se referir ao mesmo indivíduo. Em OWL, deve ser explicitamente informado que os indivíduos são iguais um ao outro, ou diferentes uns dos outros \citep{horridge2004practical}}.

\item{\textbf{Propriedades}: São as relações binárias entre os indivíduos, ou seja, elas conectam dois indivíduos. Por exemplo, a propriedade \textit{hasSibling} pode conectar o indivíduo \textit{Matthew} ao indivíduo \textit{Gemma}, ou a propriedade \textit{hasChild} pode conectar o indivíduo Peter ao indivíduo \textit{Matthew}, conforme ilustrado na figura \ref{fig:propriedade_owl}. As propriedades também podem ser inversas. Por exemplo, o inverso de \textit{hasChild} é \textit{hasParent}, conforme a Figura \ref{fig:propriedade_inversa_owl} \citep{horridge2004practical}}.

\item{\textbf{Classes}: Estas representam o conjunto de indivíduos. Elas são definidas usando descrições formais que indicam precisamente os requisitos para a adesão da classe. Por exemplo, a classe \textit{Gato} conteria todos os indivíduos que são gatos em nosso domínio de interesse. As classes também podem ser organizadas em hierarquia superclasse - subclasse, que também é conhecida como taxonomia. Por exemplo, considere as classes \textit{Animal} e \textit{Gato}. \textit{Gato} pode ser uma subclasse de \textit{Animal} (Logo \textit{Animal} é a superclasse de \textit{Gato}). Isto diz que "Todos os gatos são animais","Todos os membros da classe Gato são membros da classe Animal", "Ser um gato significa que ser um animal". A Figura \ref{fig:classe_owl} mostra uma representação de algumas classes contendo indivíduos \citep{horridge2004practical}}.

\end{itemize}

\begin{figure}
\centering
\includegraphics{imagens/propriedade_owl.png}
\caption{Representação das propriedades \citep{horridge2004practical}}
\label{fig:propriedade_owl}
\end{figure} 

\begin{figure}
\centering
\includegraphics{imagens/propriedade_inversa_owl.png}
\caption{Exemplo de propriedade Inversa \citep{horridge2004practical}}
\label{fig:propriedade_inversa_owl}
\end{figure} 

\begin{figure}
\centering
\includegraphics{imagens/classe_owl.png}
\caption{Representação das Classes (Contendo Indivíduos)
 \citep{horridge2004practical}}
\label{fig:classe_owl}
\end{figure} 

A Web Semântica pode ser aplicada em diversos contextos, inclusive em Serviços Web. Serviços Web Semânticos oferecem a oportunidade de melhorar a automação de descoberta, seleção, invocação e composição de serviços na Web. A \ac{OWL-S} é uma ontologia projetada sobre a \ac{OWL} usada para permitir ricas especificações semânticas para serem associadas com serviços Web, formalizando três tipos essenciais de conhecimento sobre um serviço \citep{Prazeres:2009:SWS:1529282.1529421}:
\begin{itemize}
  \item{O que o serviço faz?}
  \item{Como o serviço funciona?}
  \item{Como acesso o serviço?}
\end{itemize}

A \ac{OWL-S} é estruturada a partir dos três tipos de conhecimentos citados anteriormente. A classe \textit{Serviço} fornece um ponto de referência para a organização de um Serviço Web. Para cada Serviço Web existirá uma classe \textit{Serviço} da \ac{OWL-S}. As classes \textit{ServiceProfile}, \textit{ServiceModel} e \textit{ServiceGrounding} representam os três tipos de conhecimentos respectivamente. As classes são ilustradas na Figura \ref{fig:service_ontology}.

\begin{figure}
\centering
\includegraphics[scale=0.7]{imagens/service_ontology.jpg}
\caption{Nível superior das classes do OWL-S \citep{owls}}
\label{fig:service_ontology}
\end{figure} 

A classe \textit{ServiceProfile} descreve o que o serviço faz e é essencial para um agente de busca determinar se o serviço atende as necessidades. A classe é representada por uma descrição sobre o que é realizado pelo serviço, as limitações na aplicabilidade do serviço, a qualidade do mesmo e os requisitos que o solicitante deve atender para utilizar o serviço com sucesso. 

A classe \textit{ServiceModel} diz ao cliente como utilizar o serviço. Esta classe descreve como requisitar o serviço e o que acontece quando o mesmo é executado. 

A classe \textit{ServiceGrounding} especifica os detalhes de como o serviço pode ser acessado. Normalmente, esta classe irá especificar um protocolo de comunicação, os formatos de mensagens e outros detalhes mais específicos, como por exemplo, o número da porta utilizada \citep{owls}.

Um exemplo de serviço descrito em OWL-S é demonstrado no Código Fonte \ref{cod:owls}.

\begin{lstlisting}[caption=Exemplo de serviço descrito em OWL-S, label=cod:owls, language=XML, frame=single]
<profile:Profilerdf:ID="CARRO_PRECO_PROFILE">
<service:isPresentedByrdf:resource="#CARRO_PRECO_SERVICE"/>
<profile:serviceNamexml:lang="en">
 PrecodeCarro
</profile:serviceName>
<profile:textDescriptionxml:lang="en">
 Uma lista com precos de carro esta disponivel.
</profile:textDescription>
<profile:hasInputrdf:resource="#_CARRO"/>
<profile:hasOutputrdf:resource="#_PRECO"/>
<profile:has_processrdf:resource="CARRO_PRECO_PROCESS"/>
</profile:Profile>

<process:ProcessModelrdf:ID="#AUTO_PRICE_PROCESSMODEL">
<service:describesrdf:resource="#CARRO_PRECO_SERVICE"/>
<process:hasProcessrdf:resource="#CARRO_PRECO_PROCESS"/>
</process:ProcessModel>

<grounding:WsdlGroundingrdf:ID="#CARRO_PRECO_GROUNDING">
<service:supportedByrdf:resource="#CARRO_PRECO_SERVICE"/>
</grounding:WsdlGrounding>
\end{lstlisting}
