\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{xv}{chapter*.4}
\contentsline {chapter}{Lista de Tabelas}{xvii}{chapter*.5}
\contentsline {chapter}{Lista de Acr\IeC {\^o}nimos}{xix}{chapter*.6}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Servi\IeC {\c c}os Web}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Tipos de Servi\IeC {\c c}os Web}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Servi\IeC {\c c}os Web Sem\IeC {\^a}nticos}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Ontologias}{7}{subsection.2.2.1}
\contentsline {chapter}{\numberline {3}Processamento de linguagem natural}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Desambigua\IeC {\c c}\IeC {\~a}o sem\IeC {\^a}ntica}{12}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Algoritmo de Lesk}{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}WordNet}{13}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Medidas de similaridade}{14}{section.3.2}
\contentsline {chapter}{\numberline {4}Trabalhos relacionados}{17}{chapter.4}
\contentsline {chapter}{\numberline {5}Descoberta de Servi\IeC {\c c}os Web via Linguagem Natural}{19}{chapter.5}
\contentsline {section}{\numberline {5.1}Arquitetura}{19}{section.5.1}
\contentsline {section}{\numberline {5.2}Ferramentas utilizadas}{21}{section.5.2}
\contentsline {section}{\numberline {5.3}Implementa\IeC {\c c}\IeC {\~a}o}{21}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Semantic Reader}{21}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Disambiguator}{22}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Matching}{25}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Web Service Discovery}{26}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Interface}{26}{section.5.4}
\contentsline {section}{\numberline {5.5}Execu\IeC {\c c}\IeC {\~a}o}{27}{section.5.5}
\contentsline {chapter}{\numberline {6}Avalia\IeC {\c c}\IeC {\~a}o}{31}{chapter.6}
\contentsline {section}{\numberline {6.1}Performance com e sem pr\IeC {\'e}-processamento}{31}{section.6.1}
\contentsline {section}{\numberline {6.2}Avalia\IeC {\c c}\IeC {\~a}o de usu\IeC {\'a}rios}{32}{section.6.2}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~a}o}{35}{chapter.7}
\contentsline {chapter}{Refer\IeC {\^e}ncias Bibliogr\IeC {\'a}ficas}{37}{chapter.7}
\contentsline {chapter}{Ap\IeC {\^e}ndice}{39}{section*.20}
\contentsline {chapter}{\numberline {A}Formul\IeC {\'a}rio}{41}{appendix.A}
\contentsline {section}{\numberline {A.1}Formul\IeC {\'a}rio para o usu\IeC {\'a}rio do NLP Discovery}{41}{section.A.1}
